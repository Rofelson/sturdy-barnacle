# sturdy-barnacle

## Chebyshev distance
It is the maximum distance between two points in a coordinate space along any of it's dimensions. This means it is defined on real coordinate spaces, and consist of calculating the distance along each dimensions to consider the greatest.

This metric is named after Pafnuty Chebyshev.
